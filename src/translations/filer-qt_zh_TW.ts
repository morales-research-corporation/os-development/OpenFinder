<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_TW">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../about.ui" line="14"/>
        <location filename="../../build/src/ui_about.h" line="140"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../about.ui" line="37"/>
        <location filename="../../build/src/ui_about.h" line="142"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;Filer&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;Filer&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../about.ui" line="60"/>
        <location filename="../../build/src/ui_about.h" line="144"/>
        <source>The Desktop Experience</source>
        <translation>桌面體驗‎</translation>
    </message>
    <message>
        <location filename="../about.ui" line="70"/>
        <location filename="../../build/src/ui_about.h" line="145"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://github.com/helloSystem/Filer/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/helloSystem/Filer/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://github.com/helloSystem/Filer/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/helloSystem/Filer/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../about.ui" line="125"/>
        <location filename="../../build/src/ui_about.h" line="154"/>
        <source>Filer

Copyright (C) 2020-21 Simon Peter
Copyright (C) 2021 Chris Moore

Originally based on PCMan File Manager
Portions Copyright (C) 2009-14 洪任諭 (Hong Jen Yee)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Application icon

https://dribbble.com/shots/2541211--Pirate-Finder-icon#

Copyright (C) 2016 Raphael Lopes &lt;raphaellopes8@gmail.com&gt;

Used with permission of the creator https://raphaellopes.me/</source>
        <translation>Filer

版權所有 (C) 2020-21 Simon Peter
版權所有 (C) 2021 Chris Moore

原始基於 PCMan 檔管理員
部分版權 (C) 2009-14 洪任諭 (Hong Jen Yee)

此程式是免費軟體：您可以根據 GNU 通用
公共許可證條款重新分配和/或修改它，經由
自由軟體基金會中發佈;許可證的第 2 版，或
(由您選擇)任何更高的版本。

此程式的發佈是希望它將是有用的，
但沒有為此有任何保證;甚至沒有隱晦的保固商用性或
合適的特定目的。 有關詳細資訊，請查看 GNU 通用
公共許可證。

您應該會收到 GNU 通用公共許可證的副本
以及這個程式;如果沒有，請寫信致函給自由軟體基金會
51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

應用程式圖示

https://dribbble.com/shots/2541211--Pirate-Finder-icon#

版權所有 (C) 2016 Raphael Lopes &lt;raphaellopes8@gmail.com&gt;

經由建立者授權使用 https://raphaellopes.me/</translation>
    </message>
    <message>
        <source>Lightweight file manager</source>
        <translation type="vanished">輕巧的檔案管理程式</translation>
    </message>
    <message>
        <source>PCMan File Manager

Copyright (C) 2009 - 2014 洪任諭 (Hong Jen Yee)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</source>
        <translation type="vanished">PCMan 檔案管理程式

版權所有 (C) 2009-2014 洪任諭 (Hong Jen Yee)

此程式是自由軟體，您可以遵照自由軟體基金會
 ( Free Software Foundation ) 出版的 GNU 通用公共許可證條款
 ( GNU General Public License ) 第二版來修改和重新發佈
這一個程式，或者自由選擇使用任何更新的版本。

發佈此程式的目的是希望它是有用的軟體。
但沒有任何擔保，甚至沒有適合特定目的而隱含的擔保。
更詳細的情況  請參閱  GNU 通用公共許可證。


您應該已經與程式一起收到一份 GNU 通用公共許可證的副本。
如果還沒有收到，請寫信致函給:
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</translation>
    </message>
    <message>
        <source>Programming:
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;
</source>
        <translation type="vanished">程式設計:
* 洪任諭 (Hong Jen Yee - PCMan) &lt;pcman.tw@gmail.com&gt;
</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://lxqt.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://lxqt.org/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://lxqt.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://lxqt-project.org/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../about.ui" line="90"/>
        <location filename="../../build/src/ui_about.h" line="153"/>
        <source>Authors</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../about.ui" line="99"/>
        <location filename="../../build/src/ui_about.h" line="146"/>
        <source>Programming:
* Simon Peter (probono)
* Chris Moore (moochris)
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;

Application icon:
* Raphael Lopes (https://raphaellopes.me/)</source>
        <translation>程式設計：
* Simon Peter (probono)
* Chris Moore (moochris)
* 洪任諭 Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;

應用程式圖示：
* Raphael Lopes (https://raphaellopes.me/)</translation>
    </message>
    <message>
        <location filename="../about.ui" line="116"/>
        <location filename="../../build/src/ui_about.h" line="184"/>
        <source>License</source>
        <translation>授權</translation>
    </message>
</context>
<context>
    <name>AppChooserDialog</name>
    <message>
        <location filename="../app-chooser-dialog.ui" line="14"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="150"/>
        <source>Choose an Application</source>
        <translation>選擇一個應用程式</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="36"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="151"/>
        <source>Installed Applications</source>
        <translation>安裝的應用程式</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="46"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="163"/>
        <source>Custom Command</source>
        <translation>自訂指令</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="52"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="152"/>
        <source>Command line to execute:</source>
        <translation>要執行的命令列：</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="62"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="153"/>
        <source>Application name:</source>
        <translation>應用程式名稱：</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="72"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="154"/>
        <source>&lt;b&gt;These special codes can be used in the command line:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;%f&lt;/b&gt;: Represents a single file name&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%F&lt;/b&gt;: Represents multiple file names&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%u&lt;/b&gt;: Represents a single URI of the file&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%U&lt;/b&gt;: Represents multiple URIs&lt;/li&gt;
&lt;/ul&gt;</source>
        <translation>&lt;b&gt;這些特殊代號可以在指令列參數使用：&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;%f&lt;/b&gt;：代表單一檔案名稱&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%F&lt;/b&gt;：代表多個檔案名稱&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%u&lt;/b&gt;：代表單一檔案 URI&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%U&lt;/b&gt;：代表多個 URI&lt;/li&gt;
&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="91"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="161"/>
        <source>Keep terminal window open after command execution</source>
        <translation>執行指令後保持終端機視窗開啟</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="98"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="162"/>
        <source>Execute in terminal emulator</source>
        <translation>在終端機模擬器執行</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="109"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="164"/>
        <source>Set selected application as default action of this file type</source>
        <translation>將所選應用程式設定為此類型檔案的預設處理程式</translation>
    </message>
</context>
<context>
    <name>AutoRunDialog</name>
    <message>
        <location filename="../autorun.ui" line="14"/>
        <location filename="../../build/src/ui_autorun.h" line="108"/>
        <source>Removable medium is inserted</source>
        <translation>已插入外接儲存裝置</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="33"/>
        <location filename="../../build/src/ui_autorun.h" line="110"/>
        <source>&lt;b&gt;Removable medium is inserted&lt;/b&gt;</source>
        <translation>&lt;b&gt;已插入外接儲存裝置&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="40"/>
        <location filename="../../build/src/ui_autorun.h" line="111"/>
        <source>Type of medium:</source>
        <translation>儲存裝置類型：</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="47"/>
        <location filename="../../build/src/ui_autorun.h" line="112"/>
        <source>Detecting...</source>
        <translation>偵測中...</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="56"/>
        <location filename="../../build/src/ui_autorun.h" line="113"/>
        <source>Please select the action you want to perform:</source>
        <translation>請選擇想要執行的動作：</translation>
    </message>
</context>
<context>
    <name>DesktopFolder</name>
    <message>
        <location filename="../desktop-folder.ui" line="14"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="72"/>
        <source>Form</source>
        <translation>格式</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="23"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="73"/>
        <source>Desktop</source>
        <translation>桌面</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="29"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="74"/>
        <source>Desktop folder:</source>
        <translation>桌面資料夾：</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="36"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="76"/>
        <source>Image file</source>
        <translation>圖片檔案</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="42"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="81"/>
        <source>Folder path</source>
        <translation>資料夾路徑</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="49"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="82"/>
        <source>&amp;Browse</source>
        <translation>瀏覽(&amp;B)</translation>
    </message>
</context>
<context>
    <name>DesktopPreferencesDialog</name>
    <message>
        <location filename="../desktop-preferences.ui" line="14"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="205"/>
        <source>Desktop Preferences</source>
        <translation>桌面偏好設定</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="20"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="206"/>
        <source>Background</source>
        <translation>背景</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="42"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="207"/>
        <source>Wallpaper mode:</source>
        <translation>桌布：</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="55"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="208"/>
        <source>Wallpaper image file:</source>
        <translation>桌面背景圖片檔案：</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="75"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="210"/>
        <source>Background color:</source>
        <translation>‎背景顏色：</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="147"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="222"/>
        <source>Text color:</source>
        <translation>文字顏色：</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="160"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="223"/>
        <source>Shadow color:</source>
        <translation>陰影顏色：</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="173"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="224"/>
        <source>Font:</source>
        <translation>文字：</translation>
    </message>
    <message>
        <source>Select background color:</source>
        <translation type="vanished">選擇背景顏色：</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="84"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="212"/>
        <source>Image file</source>
        <translation>圖片檔案</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="90"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="217"/>
        <source>Image file path</source>
        <translation>圖片檔案路徑</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="97"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="218"/>
        <source>&amp;Browse</source>
        <translation>瀏覽(&amp;B)</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="109"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="219"/>
        <source>Label Text</source>
        <translation>標籤文字</translation>
    </message>
    <message>
        <source>Select  text color:</source>
        <translation type="vanished">選擇文字顏色：</translation>
    </message>
    <message>
        <source>Select shadow color:</source>
        <translation type="vanished">選擇陰影顏色：</translation>
    </message>
    <message>
        <source>Select font:</source>
        <translation type="vanished">選擇字型：</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="vanished">一般</translation>
    </message>
    <message>
        <source>Window Manager</source>
        <translation type="vanished">視窗管理員</translation>
    </message>
    <message>
        <source>Show menus provided by window managers when desktop is clicked</source>
        <translation type="vanished">按下桌面時顯示視窗管理員提供的選單</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="vanished">進階</translation>
    </message>
</context>
<context>
    <name>EditBookmarksDialog</name>
    <message>
        <location filename="../edit-bookmarks.ui" line="14"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="103"/>
        <source>Edit Bookmarks</source>
        <translation>編輯書籤</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="42"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="106"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="47"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="105"/>
        <source>Location</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="67"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="107"/>
        <source>&amp;Add Item</source>
        <translation>新增項目(&amp;A)</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="77"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="108"/>
        <source>&amp;Remove Item</source>
        <translation>移除項目(&amp;R)</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="102"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="109"/>
        <source>Use drag and drop to reorder the items</source>
        <translation>使用拖放重新排序項目</translation>
    </message>
</context>
<context>
    <name>ExecFileDialog</name>
    <message>
        <location filename="../exec-file.ui" line="14"/>
        <location filename="../../build/src/ui_exec-file.h" line="114"/>
        <source>Execute file</source>
        <translation>執行檔案</translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="39"/>
        <location filename="../../build/src/ui_exec-file.h" line="116"/>
        <source>&amp;Open</source>
        <translation>開啟(&amp;)</translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="52"/>
        <location filename="../../build/src/ui_exec-file.h" line="117"/>
        <source>E&amp;xecute</source>
        <translation>執行(&amp;X)</translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="62"/>
        <location filename="../../build/src/ui_exec-file.h" line="118"/>
        <source>Execute in &amp;Terminal</source>
        <translation>在終端機內執行(&amp;T)</translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="85"/>
        <location filename="../../build/src/ui_exec-file.h" line="119"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>FileOperationDialog</name>
    <message>
        <location filename="../file-operation-dialog.ui" line="25"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="124"/>
        <source>Destination:</source>
        <translation>目的地：</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="48"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="126"/>
        <source>Processing:</source>
        <translation>正在處理:</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="61"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="127"/>
        <source>Preparing...</source>
        <translation>準備中...</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="68"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="128"/>
        <source>Progress</source>
        <translation>進度</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="88"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="129"/>
        <source>Time remaining:</source>
        <translation>剩餘時間：</translation>
    </message>
</context>
<context>
    <name>FilePropsDialog</name>
    <message>
        <source>File Properties</source>
        <translation type="vanished">檔案屬性</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="133"/>
        <location filename="../../build/src/ui_file-props.h" line="365"/>
        <source>General</source>
        <translation>一般</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="208"/>
        <location filename="../../build/src/ui_file-props.h" line="372"/>
        <source>Location:</source>
        <translation>位置：</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="139"/>
        <location filename="../../build/src/ui_file-props.h" line="366"/>
        <source>File type:</source>
        <translation>檔案類型：</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="317"/>
        <location filename="../../build/src/ui_file-props.h" line="380"/>
        <source>Open with</source>
        <translation>以其他程式開啟</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="323"/>
        <location filename="../../build/src/ui_file-props.h" line="381"/>
        <source>Mime type:</source>
        <translation>Mime 類型：</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="162"/>
        <location filename="../../build/src/ui_file-props.h" line="368"/>
        <source>File size:</source>
        <translation>檔案大小：</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="20"/>
        <location filename="../../build/src/ui_file-props.h" line="362"/>
        <source>Info</source>
        <translation>內容</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="60"/>
        <location filename="../../build/src/ui_file-props.h" line="363"/>
        <source>TextLabel</source>
        <translation>文字標籤</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="185"/>
        <location filename="../../build/src/ui_file-props.h" line="370"/>
        <source>On-disk size:</source>
        <translation>磁碟上大小：</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="260"/>
        <location filename="../../build/src/ui_file-props.h" line="376"/>
        <source>Last modified:</source>
        <translation>最後修改：</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="423"/>
        <location filename="../../build/src/ui_file-props.h" line="385"/>
        <source>Everyone</source>
        <translation>任何人</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="234"/>
        <location filename="../../build/src/ui_file-props.h" line="374"/>
        <source>Link target:</source>
        <translation>連結目標：</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="346"/>
        <location filename="../../build/src/ui_file-props.h" line="383"/>
        <source>Open With:</source>
        <translation>開啟：</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="283"/>
        <location filename="../../build/src/ui_file-props.h" line="378"/>
        <source>Last accessed:</source>
        <translation>最後存取：</translation>
    </message>
    <message>
        <source>Permissions</source>
        <translation type="vanished">權限</translation>
    </message>
    <message>
        <source>Ownership</source>
        <translation type="vanished">所有權</translation>
    </message>
    <message>
        <source>Group:</source>
        <translation type="vanished">群組：</translation>
    </message>
    <message>
        <source>Owner:</source>
        <translation type="vanished">擁有者：</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="380"/>
        <location filename="../../build/src/ui_file-props.h" line="384"/>
        <source>Access Control</source>
        <translation>存取控制</translation>
    </message>
    <message>
        <source>Other:</source>
        <translation type="vanished">其他：</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="440"/>
        <location filename="../../build/src/ui_file-props.h" line="386"/>
        <source>Make the file executable</source>
        <translation>使檔案可執行</translation>
    </message>
    <message>
        <source>Read</source>
        <translation type="vanished">讀取</translation>
    </message>
    <message>
        <source>Write</source>
        <translation type="vanished">寫入</translation>
    </message>
    <message>
        <source>Execute</source>
        <translation type="vanished">執行</translation>
    </message>
    <message>
        <source>Sticky</source>
        <translation type="vanished">標籤</translation>
    </message>
    <message>
        <source>SetUID</source>
        <translation type="vanished">設置UID</translation>
    </message>
    <message>
        <source>SetGID</source>
        <translation type="vanished">設置GID</translation>
    </message>
    <message>
        <source>Advanced Mode</source>
        <translation type="vanished">進階模式</translation>
    </message>
</context>
<context>
    <name>Filer::Application</name>
    <message>
        <location filename="../application.cpp" line="189"/>
        <source>Name of configuration profile</source>
        <translation>配置設定檔名稱</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="189"/>
        <source>PROFILE</source>
        <translation>設定檔</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="192"/>
        <source>Run Filer as a daemon</source>
        <translation>將 Filer 作為守護行程</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="195"/>
        <source>Quit Filer</source>
        <translation>關閉 Filer</translation>
    </message>
    <message>
        <source>Launch desktop manager</source>
        <translation type="vanished">桌面管理程式</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="198"/>
        <source>Launch desktop manager (deprecated)</source>
        <translation>‎啟動桌面管理員（已棄用）</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="201"/>
        <source>Turn off desktop manager if it&apos;s running</source>
        <translation>桌面管理程式如果正在執行，請關閉它</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="204"/>
        <source>Open desktop preference dialog on the page with the specified name</source>
        <translation>在具有指定名稱的頁面上打開桌面偏好的對話框</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="204"/>
        <location filename="../application.cpp" line="220"/>
        <source>NAME</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="207"/>
        <source>Open new window</source>
        <translation>開啟新視窗</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="210"/>
        <source>Open Find Files utility</source>
        <translation>開啟搜尋檔案程式</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="213"/>
        <source>Set desktop wallpaper from image FILE</source>
        <translation>從圖片檔案夾設置為桌面背景</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="213"/>
        <source>FILE</source>
        <translation>檔案夾</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="217"/>
        <source>Set mode of desktop wallpaper. MODE=(color|stretch|fit|center|tile)</source>
        <translation>桌面背景的設置模式。 類別=(顏色|延展|縮放|置中|並排)</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="217"/>
        <source>MODE</source>
        <translation>類別</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="220"/>
        <source>Open Preferences dialog on the page with the specified name</source>
        <translation>使用指定名稱在頁面上打開偏好的對話框</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="223"/>
        <source>Files or directories to open</source>
        <translation>要開啟的檔案以及目錄</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="223"/>
        <source>[FILE1, FILE2,...]</source>
        <translation>[新增檔案夾1, 新增檔案夾2,...]</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="512"/>
        <location filename="../application.cpp" line="519"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="519"/>
        <source>Terminal emulator is not set.</source>
        <translation>終端機模擬器尚未設定。</translation>
    </message>
</context>
<context>
    <name>Filer::AutoRunDialog</name>
    <message>
        <location filename="../autorundialog.cpp" line="43"/>
        <source>Open in file manager</source>
        <translation>在檔案管理程式中開啟</translation>
    </message>
    <message>
        <location filename="../autorundialog.cpp" line="133"/>
        <source>Removable Disk</source>
        <translation>外接式磁碟</translation>
    </message>
</context>
<context>
    <name>Filer::DesktopPreferencesDialog</name>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="49"/>
        <source>Fill with background color only</source>
        <translation>以背景色填滿</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="50"/>
        <source>Transparent</source>
        <translation>透明度</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="51"/>
        <source>Stretch to fill the entire screen</source>
        <translation>延展到整個螢幕</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="52"/>
        <source>Stretch to fit the screen</source>
        <translation>縮放到適合螢幕大小</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="53"/>
        <source>Center on the screen</source>
        <translation>置於螢幕中央</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="54"/>
        <source>Tile the image to fill the entire screen</source>
        <translation>並排排滿整個螢幕</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="145"/>
        <source>Image Files</source>
        <translation>圖片檔</translation>
    </message>
</context>
<context>
    <name>Filer::DesktopWindow</name>
    <message>
        <location filename="../desktopwindow.cpp" line="439"/>
        <source>Stic&amp;k to Current Position</source>
        <translation>維持目前的位置(&amp;K)</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="461"/>
        <source>Desktop Preferences</source>
        <translation>桌面偏好設定</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="528"/>
        <source>Version: %1</source>
        <translation>版本：%1</translation>
    </message>
</context>
<context>
    <name>Filer::GotoFolderDialog</name>
    <message>
        <location filename="../gotofolderwindow.cpp" line="47"/>
        <source>Go</source>
        <translation>移至</translation>
    </message>
    <message>
        <location filename="../gotofolderwindow.cpp" line="48"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../gotofolderwindow.cpp" line="59"/>
        <source>Go To Folder</source>
        <translation>移至資料夾‎</translation>
    </message>
</context>
<context>
    <name>Filer::MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="210"/>
        <source>Clear text (Ctrl+K)</source>
        <translation>清除文字 (Ctrl+K)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="761"/>
        <source>Version: %1</source>
        <translation>版本：%1</translation>
    </message>
    <message>
        <source>&amp;Move to Trash</source>
        <translation type="vanished">移至垃圾桶(&amp;M)</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">刪除(&amp;D)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1464"/>
        <location filename="../mainwindow.cpp" line="1475"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1475"/>
        <source>Switch user command is not set.</source>
        <translation>切換使用者指令尚未設定。</translation>
    </message>
    <message>
        <source>Terminal emulator is not set.</source>
        <translation type="obsolete">終端機模擬器尚未設定</translation>
    </message>
</context>
<context>
    <name>Filer::PreferencesDialog</name>
    <message>
        <location filename="../preferencesdialog.cpp" line="192"/>
        <source>Icon View</source>
        <translation>圖示檢視</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="193"/>
        <source>Compact Icon View</source>
        <translation>簡易檢視</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="194"/>
        <source>Thumbnail View</source>
        <translation>縮圖檢視</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="195"/>
        <source>Detailed List View</source>
        <translation>詳細清單檢視</translation>
    </message>
</context>
<context>
    <name>Filer::TabPage</name>
    <message>
        <location filename="../tabpage.cpp" line="246"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="278"/>
        <source>%n items</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="280"/>
        <source>1 item</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="436"/>
        <source>%1 items selected</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="440"/>
        <source>%1 item selected</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source>Free space: %1 (Total: %2)</source>
        <translation type="vanished">可用空間 %1 (全部: %2)</translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="265"/>
        <source>%1 available</source>
        <translation>%1 可用</translation>
    </message>
    <message numerus="yes">
        <source>%n item(s)</source>
        <translation type="vanished">
            <numerusform>%n 個項目</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source> (%n hidden)</source>
        <translation type="vanished">
            <numerusform> (%n 個隱藏)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%1 item(s) selected</source>
        <translation type="vanished">
            <numerusform>已選取 %1 個項目</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Filer::View</name>
    <message>
        <source>Open in New T&amp;ab</source>
        <translation type="vanished">在新分頁開啟(&amp;A)</translation>
    </message>
    <message>
        <location filename="../view.cpp" line="111"/>
        <source>Open in New Win&amp;dow</source>
        <translation>在新視窗開啟(&amp;D)</translation>
    </message>
    <message>
        <location filename="../view.cpp" line="119"/>
        <source>Open in Termina&amp;l</source>
        <translation>在終端機內開啟(&amp;L)</translation>
    </message>
</context>
<context>
    <name>FindFilesDialog</name>
    <message>
        <location filename="../file-search.ui" line="14"/>
        <source>Find Files</source>
        <translation>搜尋檔案</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="24"/>
        <source>Name/Location</source>
        <translation>名稱/位置</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="30"/>
        <source>File name patterns</source>
        <translation>‎檔案名稱樣式</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="36"/>
        <source>Pattern:</source>
        <translation>樣式：</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="46"/>
        <location filename="../file-search.ui" line="221"/>
        <source>Case insensitive</source>
        <translation>不分大小寫</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="53"/>
        <location filename="../file-search.ui" line="228"/>
        <source>Use regular expression</source>
        <translation>依順序排列</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="63"/>
        <source>Places to search</source>
        <translation>搜尋位置</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="76"/>
        <source>Add</source>
        <translation>新增</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="88"/>
        <source>Remove</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="117"/>
        <source>Search in sub directories</source>
        <translation>在子目錄中搜尋</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="124"/>
        <source>Search hidden files</source>
        <translation>搜尋隱藏的檔案</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="135"/>
        <location filename="../file-search.ui" line="141"/>
        <source>File Type</source>
        <translation>檔案類型</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="147"/>
        <source>Only search for files of following types:</source>
        <translation>只搜尋以下類型的檔案：</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="154"/>
        <source>Text files</source>
        <translation>文字檔</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="161"/>
        <source>Image files</source>
        <translation>圖片檔</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="168"/>
        <source>Audio files</source>
        <translation>音訊檔</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="175"/>
        <source>Video files</source>
        <translation>影像檔</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="182"/>
        <source>Documents</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="206"/>
        <source>Content</source>
        <translation>內容</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="212"/>
        <source>File contains</source>
        <translation>‎檔案包含‎</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="252"/>
        <source>Info</source>
        <translation>‎資訊‎</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="258"/>
        <source>File Size</source>
        <translation>檔案大小</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="264"/>
        <source>Bigger than:</source>
        <translation>‎大於：</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="289"/>
        <source>Smaller than:</source>
        <translation>小於：</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="317"/>
        <source>Last Modified Time</source>
        <translation>‎上次修改時間‎</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="323"/>
        <source>Earlier than:</source>
        <translation>早於：</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="340"/>
        <source>Later than:</source>
        <translation>晚於：</translation>
    </message>
</context>
<context>
    <name>Fm::AppChooserComboBox</name>
    <message>
        <location filename="../appchoosercombobox.cpp" line="79"/>
        <source>Customize</source>
        <translation>自訂</translation>
    </message>
</context>
<context>
    <name>Fm::AppChooserDialog</name>
    <message>
        <location filename="../appchooserdialog.cpp" line="262"/>
        <source>Select an application to open &quot;%1&quot; files</source>
        <translation>選取用來開啟 &quot;%1&quot; 檔案的應用程式</translation>
    </message>
</context>
<context>
    <name>Fm::CreateNewMenu</name>
    <message>
        <location filename="../createnewmenu.cpp" line="29"/>
        <source>Folder</source>
        <translation>資料夾</translation>
    </message>
    <message>
        <location filename="../createnewmenu.cpp" line="33"/>
        <source>Blank File</source>
        <translation>空白檔案</translation>
    </message>
</context>
<context>
    <name>Fm::DirTreeModel</name>
    <message>
        <location filename="../dirtreemodelitem.cpp" line="77"/>
        <source>Loading...</source>
        <translation>載入中...</translation>
    </message>
    <message>
        <location filename="../dirtreemodelitem.cpp" line="208"/>
        <source>&lt;No sub folders&gt;</source>
        <translation>&lt;沒有子資料夾&gt;</translation>
    </message>
</context>
<context>
    <name>Fm::DirTreeView</name>
    <message>
        <source>Open in New T&amp;ab</source>
        <translation type="vanished">在新分頁中打開(&amp;A)</translation>
    </message>
    <message>
        <location filename="../dirtreeview.cpp" line="220"/>
        <source>Open in New Win&amp;dow</source>
        <translation>在新視窗中打開(&amp;D)</translation>
    </message>
    <message>
        <location filename="../dirtreeview.cpp" line="226"/>
        <source>Open in Termina&amp;l</source>
        <translation>在終端機中打開(&amp;L)</translation>
    </message>
</context>
<context>
    <name>Fm::DndActionMenu</name>
    <message>
        <location filename="../dndactionmenu.cpp" line="26"/>
        <source>Copy here</source>
        <translation>複製到這裡</translation>
    </message>
    <message>
        <location filename="../dndactionmenu.cpp" line="27"/>
        <source>Move here</source>
        <translation>移動到這裡</translation>
    </message>
    <message>
        <location filename="../dndactionmenu.cpp" line="28"/>
        <source>Create symlink here</source>
        <translation>建立符號連結到這裡</translation>
    </message>
    <message>
        <location filename="../dndactionmenu.cpp" line="30"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>Fm::EditBookmarksDialog</name>
    <message>
        <location filename="../editbookmarksdialog.cpp" line="96"/>
        <source>New bookmark</source>
        <translation>新書籤</translation>
    </message>
</context>
<context>
    <name>Fm::ExecFileDialog</name>
    <message>
        <location filename="../execfiledialog.cpp" line="40"/>
        <source>This text file &apos;%1&apos; seems to be an executable script.
What do you want to do with it?</source>
        <translation>這個文字檔 &apos;%1&apos; 似乎是可執行的 script。
想要進行什麼操作？</translation>
    </message>
    <message>
        <location filename="../execfiledialog.cpp" line="45"/>
        <source>This file &apos;%1&apos; is executable. Do you want to execute it?</source>
        <translation>這個檔案 &apos;%1&apos; 是可執行檔，是否想要執行？</translation>
    </message>
</context>
<context>
    <name>Fm::FileMenu</name>
    <message>
        <location filename="../filemenu.cpp" line="93"/>
        <source>Open</source>
        <translation>開啟</translation>
    </message>
    <message>
        <source>OpenWith</source>
        <translation type="obsolete">用其他程式開啟</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="136"/>
        <source>Open With...</source>
        <translation>用其他程式開啟...</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="164"/>
        <source>Other Applications</source>
        <translation>其他應用程式</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="170"/>
        <source>Create &amp;New</source>
        <translation>新建(&amp;N)</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="128"/>
        <source>&amp;Restore</source>
        <translation>恢復(&amp;R)</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="99"/>
        <source>Show Contents</source>
        <translation>‎顯示內容‎</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="177"/>
        <source>Cut</source>
        <translation>剪下</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="181"/>
        <source>Copy</source>
        <translation>複製</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="185"/>
        <source>Paste</source>
        <translation>貼上</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="190"/>
        <source>&amp;Move to Trash</source>
        <translation>移動到垃圾桶(&amp;M)</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="242"/>
        <source>Get Info</source>
        <translation>取得資訊‎</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="247"/>
        <source>&amp;Empty Trash</source>
        <translation>清空垃圾桶(&amp;E)</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="349"/>
        <source>Output</source>
        <translation>輸出</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">刪除(&amp;D)</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">刪除</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="194"/>
        <source>Rename</source>
        <translation>重新命名</translation>
    </message>
    <message>
        <source>Extract to...</source>
        <translation type="vanished">解壓縮到...</translation>
    </message>
    <message>
        <source>Extract Here</source>
        <translation type="vanished">在此解壓縮</translation>
    </message>
    <message>
        <source>Compress</source>
        <translation type="vanished">壓縮</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="vanished">屬性</translation>
    </message>
</context>
<context>
    <name>Fm::FileOperation</name>
    <message>
        <location filename="../fileoperation.cpp" line="221"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="222"/>
        <source>Some files cannot be moved to trash can because the underlying file systems don&apos;t support this operation.
Do you want to delete them instead?</source>
        <translation>因為檔案系統不支援，有些檔案無法丟到垃圾桶
是否直接刪除這些檔案？</translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="263"/>
        <location filename="../fileoperation.cpp" line="279"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="264"/>
        <source>Do you want to delete the selected files?</source>
        <translation>你確定要刪除選取的檔案嗎?</translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="280"/>
        <source>Do you want to move the selected files to trash can?</source>
        <translation>你確定要把選取的檔案移到垃圾桶嗎?</translation>
    </message>
</context>
<context>
    <name>Fm::FileOperationDialog</name>
    <message>
        <location filename="../fileoperationdialog.cpp" line="41"/>
        <source>Move files</source>
        <translation>移動檔案</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="42"/>
        <source>Moving the following files to destination folder:</source>
        <translation>正在移動以下檔案到目的資料夾：</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="45"/>
        <source>Copy Files</source>
        <translation>複製檔案</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="46"/>
        <source>Copying the following files to destination folder:</source>
        <translation>正在複製以下檔案到目的資料夾：</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="49"/>
        <source>Trash Files</source>
        <translation>將檔案丟到垃圾桶</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="50"/>
        <source>Moving the following files to trash can:</source>
        <translation>正在將以下檔案移到垃圾桶：</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="53"/>
        <source>Delete Files</source>
        <translation>刪除檔案</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="54"/>
        <source>Deleting the following files</source>
        <translation>正在刪除以下檔案</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="59"/>
        <source>Create Symlinks</source>
        <translation>建立符號連結</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="60"/>
        <source>Creating symlinks for the following files:</source>
        <translation>正在建立以下檔案的符號連結：</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="63"/>
        <source>Change Attributes</source>
        <translation>改變屬性</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="64"/>
        <source>Changing attributes of the following files:</source>
        <translation>正在變更以下檔案的屬性：</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="69"/>
        <source>Restore Trashed Files</source>
        <translation>恢復被刪除的檔案</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="70"/>
        <source>Restoring the following files from trash can:</source>
        <translation>正在恢復以下被刪除的檔案：</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="139"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
</context>
<context>
    <name>Fm::FilePropsDialog</name>
    <message>
        <location filename="../filepropsdialog.cpp" line="148"/>
        <source>View folder content</source>
        <translation>檢視資料夾內容</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="149"/>
        <source>View and modify folder content</source>
        <translation>檢視及修改資料夾內容</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="153"/>
        <source>Read</source>
        <translation>讀取</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="154"/>
        <source>Read and write</source>
        <translation>讀取及寫入</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="156"/>
        <source>Forbidden</source>
        <translation>禁止</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="274"/>
        <source>Files of different types</source>
        <translation>不同類型的檔案</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="304"/>
        <source>Multiple Files</source>
        <translation>多個檔案</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="432"/>
        <source>Apply changes</source>
        <translation>套用變更</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="433"/>
        <source>Do you want to recursively apply these changes to all files and sub-folders?</source>
        <translation>你是否想將這些變更套用到所有子資料夾和其內的檔案?</translation>
    </message>
</context>
<context>
    <name>Fm::FileSearchDialog</name>
    <message>
        <location filename="../filesearchdialog.cpp" line="120"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../filesearchdialog.cpp" line="120"/>
        <source>You should add at least add one directory to search.</source>
        <translation>您應該至少添加一個要搜尋的目錄。</translation>
    </message>
    <message>
        <location filename="../filesearchdialog.cpp" line="127"/>
        <source>Select a folder</source>
        <translation>選擇資料夾</translation>
    </message>
</context>
<context>
    <name>Fm::FolderMenu</name>
    <message>
        <location filename="../foldermenu.cpp" line="37"/>
        <source>Create &amp;New</source>
        <translation>新建(&amp;N)</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="44"/>
        <source>&amp;Paste</source>
        <translation>貼上(&amp;P)</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="50"/>
        <source>Select &amp;All</source>
        <translation>全選(&amp;A)</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="54"/>
        <source>Invert Selection</source>
        <translation>反向選取</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="60"/>
        <source>Sorting</source>
        <translation>排序</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="65"/>
        <source>Show Hidden</source>
        <translation>顯示隱藏檔</translation>
    </message>
    <message>
        <source>Folder Pr&amp;operties</source>
        <translation type="vanished">資料夾屬性(&amp;O)</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation type="vanished">資料夾</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="obsolete">檔案</translation>
    </message>
    <message>
        <source>Blank File</source>
        <translation type="vanished">空白檔案</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="73"/>
        <source>Get Info</source>
        <translation>取得資訊‎</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="99"/>
        <source>By File Name</source>
        <translation>依照檔名</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="100"/>
        <source>By Modification Time</source>
        <translation>依照修改時間</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="101"/>
        <source>By File Size</source>
        <translation>依照檔案大小</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="102"/>
        <source>By File Type</source>
        <translation>依照檔案型態</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="103"/>
        <source>By File Owner</source>
        <translation>依照檔案所有者</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="115"/>
        <source>Ascending</source>
        <translation>遞增排列</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="120"/>
        <source>Descending</source>
        <translation>遞減排列</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="135"/>
        <source>Folder First</source>
        <translation>資料夾優先</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="144"/>
        <source>Case Sensitive</source>
        <translation>區分大小寫</translation>
    </message>
</context>
<context>
    <name>Fm::FolderModel</name>
    <message>
        <location filename="../foldermodel.cpp" line="313"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="316"/>
        <source>Type</source>
        <translation>類型</translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="319"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="322"/>
        <source>Modified</source>
        <translation>修改</translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="325"/>
        <source>Owner</source>
        <translation>所有者</translation>
    </message>
</context>
<context>
    <name>Fm::FontButton</name>
    <message>
        <location filename="../fontbutton.cpp" line="46"/>
        <source>Bold</source>
        <translation>粗體</translation>
    </message>
    <message>
        <location filename="../fontbutton.cpp" line="50"/>
        <source>Italic</source>
        <translation>斜體</translation>
    </message>
</context>
<context>
    <name>Fm::MountOperationPasswordDialog</name>
    <message>
        <location filename="../mountoperationpassworddialog.cpp" line="40"/>
        <source>&amp;Connect</source>
        <translation>連接(&amp;C)</translation>
    </message>
</context>
<context>
    <name>Fm::PlacesModel</name>
    <message>
        <location filename="../placesmodel.cpp" line="82"/>
        <source>Places</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="90"/>
        <source>Desktop</source>
        <translation>桌面</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="212"/>
        <source>Trash</source>
        <translation>垃圾桶</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="99"/>
        <source>Computer</source>
        <translation>電腦</translation>
    </message>
    <message>
        <source>Applications</source>
        <translation type="vanished">應用程式</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="126"/>
        <source>Network</source>
        <translation>網路</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="40"/>
        <source>Devices</source>
        <translation>裝置</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="137"/>
        <source>Bookmarks</source>
        <translation>書籤</translation>
    </message>
</context>
<context>
    <name>Fm::PlacesView</name>
    <message>
        <location filename="../placesview.cpp" line="362"/>
        <source>Empty Trash</source>
        <translation>清空垃圾桶</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">重新命名</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">刪除</translation>
    </message>
    <message>
        <source>Open in New Tab</source>
        <translation type="vanished">在新分頁中打開</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="351"/>
        <source>Open in New Window</source>
        <translation>在新視窗中打開</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="371"/>
        <source>Move Bookmark Up</source>
        <translation>上移書籤</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="376"/>
        <source>Move Bookmark Down</source>
        <translation>下移書籤</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="380"/>
        <source>Rename Bookmark</source>
        <translation>重新命名書籤</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="383"/>
        <source>Remove Bookmark</source>
        <translation>移除書籤</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="392"/>
        <location filename="../placesview.cpp" line="409"/>
        <source>Unmount</source>
        <translation>卸載</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="396"/>
        <source>Mount</source>
        <translation>掛載</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="402"/>
        <source>Eject</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>Fm::RenameDialog</name>
    <message>
        <location filename="../renamedialog.cpp" line="50"/>
        <location filename="../renamedialog.cpp" line="69"/>
        <source>Type: %1
Size: %2
Modified: %3</source>
        <translation>類型：%1
大小：%2
最後修改：%3</translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="56"/>
        <source>Type: %1
Modified: %2</source>
        <translation>類型：%1
最後修改：%2</translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="75"/>
        <source>Type: %1
Modified: %3</source>
        <translation>類型：%1
最後修改：%3</translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="89"/>
        <source>&amp;Overwrite</source>
        <translation>覆蓋(&amp;O)</translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="91"/>
        <source>&amp;Rename</source>
        <translation>重新命名(&amp;R)</translation>
    </message>
</context>
<context>
    <name>Fm::SidePane</name>
    <message>
        <location filename="../sidepane.cpp" line="49"/>
        <location filename="../sidepane.cpp" line="133"/>
        <source>Places</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../sidepane.cpp" line="50"/>
        <location filename="../sidepane.cpp" line="135"/>
        <source>Directory Tree</source>
        <translation>樹狀目錄</translation>
    </message>
    <message>
        <location filename="../sidepane.cpp" line="143"/>
        <source>Shows list of common places, devices, and bookmarks in sidebar</source>
        <translation>在側邊欄中顯示常用位置、裝置和書籤列表</translation>
    </message>
    <message>
        <location filename="../sidepane.cpp" line="145"/>
        <source>Shows tree of directories in sidebar</source>
        <translation>在側邊欄中顯示樹狀目錄</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../main-win.ui" line="14"/>
        <location filename="../../build/src/ui_main-win.h" line="602"/>
        <source>File Manager</source>
        <translation>資料管理程式</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="274"/>
        <location filename="../../build/src/ui_main-win.h" line="605"/>
        <source>Go Up</source>
        <translation>往上一層</translation>
    </message>
    <message>
        <source>Home</source>
        <translation type="vanished">家目錄</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="426"/>
        <location filename="../../build/src/ui_main-win.h" line="650"/>
        <source>Reload</source>
        <translation>重新讀取</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="271"/>
        <location filename="../../build/src/ui_main-win.h" line="603"/>
        <source>Go &amp;Up</source>
        <translation>上一層(&amp;U)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="277"/>
        <location filename="../../build/src/ui_main-win.h" line="608"/>
        <source>Ctrl+Up</source>
        <translation>Ctrl+Up</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="286"/>
        <location filename="../../build/src/ui_main-win.h" line="610"/>
        <source>&amp;Home</source>
        <translation>家目錄(&amp;H)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="289"/>
        <location filename="../../build/src/ui_main-win.h" line="612"/>
        <source>Ctrl+Shift+H</source>
        <translation>Ctrl+Shift+H</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="298"/>
        <location filename="../../build/src/ui_main-win.h" line="614"/>
        <source>&amp;Reload</source>
        <translation>重新讀取(&amp;R)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="301"/>
        <location filename="../../build/src/ui_main-win.h" line="616"/>
        <source>Ctrl+Shift+R</source>
        <translation>Ctrl+Shift+R</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="313"/>
        <location filename="../../build/src/ui_main-win.h" line="618"/>
        <source>Go</source>
        <translation>執行</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="325"/>
        <location filename="../../build/src/ui_main-win.h" line="619"/>
        <source>Quit</source>
        <translation>離開</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation type="vanished">關於(&amp;A)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="343"/>
        <location filename="../../build/src/ui_main-win.h" line="621"/>
        <source>&amp;New Window</source>
        <translation>新視窗(&amp;N)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="346"/>
        <location filename="../../build/src/ui_main-win.h" line="623"/>
        <source>New Window</source>
        <translation>新視窗</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="349"/>
        <location filename="../../build/src/ui_main-win.h" line="626"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="357"/>
        <location filename="../../build/src/ui_main-win.h" line="628"/>
        <source>Show &amp;Hidden</source>
        <translation>顯示隱藏檔(&amp;H)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="366"/>
        <location filename="../../build/src/ui_main-win.h" line="629"/>
        <source>&amp;Computer</source>
        <translation>電腦(&amp;C)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="369"/>
        <location filename="../../build/src/ui_main-win.h" line="631"/>
        <source>Ctrl+Shift+C</source>
        <translation>Ctrl+Shift+C</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="378"/>
        <location filename="../../build/src/ui_main-win.h" line="633"/>
        <source>&amp;Trash</source>
        <translation>垃圾桶(&amp;T)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="381"/>
        <location filename="../../build/src/ui_main-win.h" line="635"/>
        <source>Ctrl+Shift+T</source>
        <translation>Ctrl+Shift+T</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="386"/>
        <location filename="../../build/src/ui_main-win.h" line="637"/>
        <source>&amp;Network</source>
        <translation>網路(&amp;N)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="398"/>
        <location filename="../../build/src/ui_main-win.h" line="641"/>
        <source>&amp;Desktop</source>
        <translation>桌面(&amp;D)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="401"/>
        <location filename="../../build/src/ui_main-win.h" line="643"/>
        <source>Ctrl+Shift+D</source>
        <translation>Ctrl+Shift+D</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="410"/>
        <location filename="../../build/src/ui_main-win.h" line="645"/>
        <source>&amp;Add to Bookmarks</source>
        <translation>加入到書籤(&amp;A)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="415"/>
        <location filename="../../build/src/ui_main-win.h" line="646"/>
        <source>&amp;Applications</source>
        <translation>程式(&amp;A)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="418"/>
        <location filename="../../build/src/ui_main-win.h" line="648"/>
        <source>Ctrl+Shift+A</source>
        <translation>Ctrl+Shift+A</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="434"/>
        <location filename="../../build/src/ui_main-win.h" line="651"/>
        <source>&amp;Icon View</source>
        <translation>圖示檢視(&amp;I)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="437"/>
        <location filename="../../build/src/ui_main-win.h" line="653"/>
        <source>Ctrl+1</source>
        <translation>Ctrl+1</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="445"/>
        <location filename="../../build/src/ui_main-win.h" line="655"/>
        <source>&amp;Compact View</source>
        <translation>簡易檢視(&amp;C)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="453"/>
        <location filename="../../build/src/ui_main-win.h" line="656"/>
        <source>&amp;Detailed List</source>
        <translation>詳細清單(&amp;D)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="456"/>
        <location filename="../../build/src/ui_main-win.h" line="658"/>
        <source>Ctrl+2</source>
        <translation>Ctrl+2</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="464"/>
        <location filename="../../build/src/ui_main-win.h" line="660"/>
        <source>&amp;Thumbnail View</source>
        <translation>縮圖檢視(&amp;T)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="473"/>
        <location filename="../../build/src/ui_main-win.h" line="661"/>
        <source>Cu&amp;t</source>
        <translation>剪下(&amp;T)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="521"/>
        <location filename="../../build/src/ui_main-win.h" line="678"/>
        <source>&amp;Ascending</source>
        <translation>遞增排列(&amp;A)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="529"/>
        <location filename="../../build/src/ui_main-win.h" line="679"/>
        <source>&amp;Descending</source>
        <translation>遞減排列(&amp;D)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="537"/>
        <location filename="../../build/src/ui_main-win.h" line="680"/>
        <source>&amp;By File Name</source>
        <translation>按檔案名稱(&amp;B)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="545"/>
        <location filename="../../build/src/ui_main-win.h" line="681"/>
        <source>By &amp;Modification Time</source>
        <translation>按修改時間(&amp;M)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="553"/>
        <location filename="../../build/src/ui_main-win.h" line="682"/>
        <source>By File &amp;Type</source>
        <translation>按檔案類型(&amp;T)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="561"/>
        <location filename="../../build/src/ui_main-win.h" line="683"/>
        <source>By &amp;Owner</source>
        <translation>按擁有者(&amp;O)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="569"/>
        <location filename="../../build/src/ui_main-win.h" line="684"/>
        <source>&amp;Folder First</source>
        <translation>檔案夾優先(&amp;F)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="640"/>
        <location filename="../../build/src/ui_main-win.h" line="707"/>
        <source>&amp;Move to Trash</source>
        <translation>移動到垃圾桶(&amp;M)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="643"/>
        <location filename="../../build/src/ui_main-win.h" line="709"/>
        <source>Ctrl+Backspace</source>
        <translation>Ctrl+Backspace</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="656"/>
        <location filename="../../build/src/ui_main-win.h" line="715"/>
        <source>C&amp;lose Tab</source>
        <translation>關閉(&amp;L)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="760"/>
        <location filename="../../build/src/ui_main-win.h" line="746"/>
        <source>&amp;Go To Folder</source>
        <translation>移至資料夾(&amp;G)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="763"/>
        <location filename="../../build/src/ui_main-win.h" line="748"/>
        <source>Go To Folder</source>
        <translation>移至資料夾‎</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="766"/>
        <location filename="../../build/src/ui_main-win.h" line="751"/>
        <source>Ctrl+Shift+G</source>
        <translation>Ctrl+Shift+G</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="771"/>
        <location filename="../../build/src/ui_main-win.h" line="753"/>
        <source>&amp;Downloads</source>
        <translation>‎下載‎(&amp;D)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="774"/>
        <location filename="../../build/src/ui_main-win.h" line="755"/>
        <source>Downloads</source>
        <translation>‎下載‎</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="777"/>
        <location filename="../../build/src/ui_main-win.h" line="758"/>
        <source>Ctrl+Shift+L</source>
        <translation>Ctrl+Shift+L</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="785"/>
        <location filename="../../build/src/ui_main-win.h" line="760"/>
        <source>&amp;Utilities</source>
        <translation>工具程式(&amp;U)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="788"/>
        <location filename="../../build/src/ui_main-win.h" line="762"/>
        <source>Utilities</source>
        <translation>工具程式</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="791"/>
        <location filename="../../build/src/ui_main-win.h" line="765"/>
        <source>Ctrl+Shift+U</source>
        <translation>Ctrl+Shift+U</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="799"/>
        <location filename="../../build/src/ui_main-win.h" line="767"/>
        <source>&amp;Documents</source>
        <translation>文件(&amp;D)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="802"/>
        <location filename="../../build/src/ui_main-win.h" line="769"/>
        <source>Documents</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="805"/>
        <location filename="../../build/src/ui_main-win.h" line="772"/>
        <source>Ctrl+Shift+O</source>
        <translation>Ctrl+Shift+O</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="813"/>
        <location filename="../../build/src/ui_main-win.h" line="774"/>
        <source>Open</source>
        <translation>開啟</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="816"/>
        <location filename="../../build/src/ui_main-win.h" line="776"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="821"/>
        <location filename="../../build/src/ui_main-win.h" line="778"/>
        <source>&amp;Duplicate</source>
        <translation>再製(&amp;D)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="824"/>
        <location filename="../../build/src/ui_main-win.h" line="780"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="829"/>
        <location filename="../../build/src/ui_main-win.h" line="782"/>
        <source>Empty Trash</source>
        <translation>清空垃圾桶</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="832"/>
        <location filename="../../build/src/ui_main-win.h" line="784"/>
        <source>Ctrl+Alt+Backspace</source>
        <translation>Ctrl+Alt+Backspace</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="837"/>
        <location filename="../../build/src/ui_main-win.h" line="786"/>
        <source>Show Contents</source>
        <translation>‎顯示內容‎</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="840"/>
        <location filename="../../build/src/ui_main-win.h" line="788"/>
        <source>Ctrl+Alt+O</source>
        <translation>Ctrl+Alt+O</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="849"/>
        <location filename="../main-win.ui" line="852"/>
        <location filename="../../build/src/ui_main-win.h" line="790"/>
        <location filename="../../build/src/ui_main-win.h" line="792"/>
        <source>Go Up and Close Current</source>
        <translation>往上以及關閉目前的視窗</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="855"/>
        <location filename="../../build/src/ui_main-win.h" line="795"/>
        <source>Ctrl+Shift+Up</source>
        <translation>Ctrl+Shift+Up</translation>
    </message>
    <message>
        <source>File &amp;Properties</source>
        <translation type="vanished">檔案屬性(&amp;P)</translation>
    </message>
    <message>
        <source>&amp;Folder Properties</source>
        <translation type="vanished">資料夾屬性(&amp;F)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="672"/>
        <location filename="../../build/src/ui_main-win.h" line="720"/>
        <source>&amp;Case Sensitive</source>
        <translation>區分大小寫(&amp;C)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="680"/>
        <location filename="../../build/src/ui_main-win.h" line="721"/>
        <source>By File &amp;Size</source>
        <translation>按檔案大小(&amp;S)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="685"/>
        <location filename="../../build/src/ui_main-win.h" line="722"/>
        <source>&amp;Close Window</source>
        <translation>關閉視窗(&amp;C)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="389"/>
        <location filename="../main-win.ui" line="723"/>
        <location filename="../../build/src/ui_main-win.h" line="639"/>
        <location filename="../../build/src/ui_main-win.h" line="735"/>
        <source>Ctrl+Shift+N</source>
        <translation>Ctrl+Shift+N</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="334"/>
        <location filename="../../build/src/ui_main-win.h" line="620"/>
        <source>&amp;About Filer</source>
        <translation>關於 Filer(&amp;A)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="661"/>
        <location filename="../../build/src/ui_main-win.h" line="716"/>
        <source>Get &amp;Info</source>
        <translation>取得資訊(&amp;I)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="664"/>
        <location filename="../../build/src/ui_main-win.h" line="718"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="735"/>
        <location filename="../../build/src/ui_main-win.h" line="739"/>
        <source>Ctrl+Alt+N</source>
        <translation>Ctrl+Alt+N</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="743"/>
        <location filename="../../build/src/ui_main-win.h" line="743"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="751"/>
        <location filename="../../build/src/ui_main-win.h" line="745"/>
        <source>Filter</source>
        <translation>過濾</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="121"/>
        <location filename="../../build/src/ui_main-win.h" line="799"/>
        <source>C&amp;reate New</source>
        <translation>建立新的(&amp;L)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="150"/>
        <location filename="../../build/src/ui_main-win.h" line="802"/>
        <source>&amp;Sorting</source>
        <translation>排序(&amp;S)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="239"/>
        <location filename="../../build/src/ui_main-win.h" line="807"/>
        <source>Main Toolbar</source>
        <translation>工具列</translation>
    </message>
    <message>
        <source>Icon View</source>
        <translation type="obsolete">圖示檢視</translation>
    </message>
    <message>
        <source>Compact View</source>
        <translation type="obsolete">簡易檢視</translation>
    </message>
    <message>
        <source>Detailed List</source>
        <translation type="obsolete">詳細清單</translation>
    </message>
    <message>
        <source>Thumbnail View</source>
        <translation type="obsolete">縮圖檢視</translation>
    </message>
    <message>
        <source>&amp;Cut</source>
        <translation type="obsolete">剪下(&amp;C)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="476"/>
        <location filename="../../build/src/ui_main-win.h" line="663"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="485"/>
        <location filename="../../build/src/ui_main-win.h" line="665"/>
        <source>&amp;Copy</source>
        <translation>複製(&amp;C)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="488"/>
        <location filename="../../build/src/ui_main-win.h" line="667"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="497"/>
        <location filename="../../build/src/ui_main-win.h" line="669"/>
        <source>&amp;Paste</source>
        <translation>貼上(&amp;P)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="500"/>
        <location filename="../../build/src/ui_main-win.h" line="671"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="505"/>
        <location filename="../../build/src/ui_main-win.h" line="673"/>
        <source>Select &amp;All</source>
        <translation>全選(&amp;A)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="513"/>
        <location filename="../../build/src/ui_main-win.h" line="677"/>
        <source>Pr&amp;eferences</source>
        <translation>偏好設定(&amp;R)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="631"/>
        <location filename="../../build/src/ui_main-win.h" line="706"/>
        <source>&amp;Invert Selection</source>
        <translation>反向選取(&amp;I)</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">刪除(&amp;D)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="648"/>
        <location filename="../../build/src/ui_main-win.h" line="711"/>
        <source>&amp;Rename</source>
        <translation>重新命名(&amp;R)</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="obsolete">全選</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="508"/>
        <location filename="../../build/src/ui_main-win.h" line="675"/>
        <source>Ctrl+A</source>
        <translation></translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation type="obsolete">偏好設定</translation>
    </message>
    <message>
        <source>Ascending</source>
        <translation type="vanished">升冪排列</translation>
    </message>
    <message>
        <source>Descending</source>
        <translation type="vanished">降冪排列</translation>
    </message>
    <message>
        <source>By File Name</source>
        <translation type="vanished">依照檔名</translation>
    </message>
    <message>
        <source>By Modification Time</source>
        <translation type="vanished">依照修改時間</translation>
    </message>
    <message>
        <source>By File Type</source>
        <translation type="vanished">依照檔案類型</translation>
    </message>
    <message>
        <source>By Owner</source>
        <translation type="vanished">依照檔案擁有者</translation>
    </message>
    <message>
        <source>Folder First</source>
        <translation type="vanished">資料夾優先</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="578"/>
        <location filename="../../build/src/ui_main-win.h" line="685"/>
        <source>New &amp;Tab</source>
        <translation>新分頁(&amp;T)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="581"/>
        <location filename="../../build/src/ui_main-win.h" line="687"/>
        <source>New Tab</source>
        <translation>新分頁</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="584"/>
        <location filename="../main-win.ui" line="701"/>
        <location filename="../../build/src/ui_main-win.h" line="690"/>
        <location filename="../../build/src/ui_main-win.h" line="729"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="596"/>
        <location filename="../../build/src/ui_main-win.h" line="692"/>
        <source>Go &amp;Back</source>
        <translation>後退(&amp;B)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="599"/>
        <location filename="../../build/src/ui_main-win.h" line="694"/>
        <source>Go Back</source>
        <translation>後退</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="602"/>
        <location filename="../../build/src/ui_main-win.h" line="697"/>
        <source>Alt+Left</source>
        <translation>Alt+左鍵</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="611"/>
        <location filename="../../build/src/ui_main-win.h" line="699"/>
        <source>Go &amp;Forward</source>
        <translation>前進(&amp;F)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="614"/>
        <location filename="../../build/src/ui_main-win.h" line="701"/>
        <source>Go Forward</source>
        <translation>前進</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="617"/>
        <location filename="../../build/src/ui_main-win.h" line="704"/>
        <source>Alt+Right</source>
        <translation>Alt+右鍵</translation>
    </message>
    <message>
        <source>Invert Selection</source>
        <translation type="obsolete">反向選取</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">刪除</translation>
    </message>
    <message>
        <source>Del</source>
        <translation type="vanished">Del</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="obsolete">重新命名</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="651"/>
        <location filename="../../build/src/ui_main-win.h" line="713"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <source>Close Tab</source>
        <translation type="obsolete">關閉分頁</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="688"/>
        <location filename="../../build/src/ui_main-win.h" line="724"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <source>File Properties</source>
        <translation type="obsolete">檔案屬性</translation>
    </message>
    <message>
        <source>Alt+Return</source>
        <translation type="vanished">Alt+Return\Enter</translation>
    </message>
    <message>
        <source>Folder Properties</source>
        <translation type="obsolete">資料夾屬性</translation>
    </message>
    <message>
        <source>Case Sensitive</source>
        <translation type="vanished">區分大小寫</translation>
    </message>
    <message>
        <source>By File Size</source>
        <translation type="vanished">依照檔案大小</translation>
    </message>
    <message>
        <source>Close Window</source>
        <translation type="vanished">關閉視窗</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="693"/>
        <location filename="../../build/src/ui_main-win.h" line="726"/>
        <source>Edit Bookmarks</source>
        <translation>編輯書籤</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="698"/>
        <location filename="../../build/src/ui_main-win.h" line="727"/>
        <source>Open &amp;Terminal</source>
        <translation>開啟終端機(&amp;T)</translation>
    </message>
    <message>
        <source>F4</source>
        <translation type="vanished">F4</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="706"/>
        <location filename="../../build/src/ui_main-win.h" line="731"/>
        <source>Open as &amp;Root</source>
        <translation>以系統管理員權限開啟(&amp;R)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="711"/>
        <location filename="../../build/src/ui_main-win.h" line="732"/>
        <source>&amp;Edit Bookmarks</source>
        <translation>編輯書籤(&amp;E)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="720"/>
        <location filename="../../build/src/ui_main-win.h" line="733"/>
        <source>&amp;Folder</source>
        <translation>資料夾 (&amp;F)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="732"/>
        <location filename="../../build/src/ui_main-win.h" line="737"/>
        <source>&amp;Blank File</source>
        <translation>空白檔案(&amp;B)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="740"/>
        <location filename="../../build/src/ui_main-win.h" line="741"/>
        <source>&amp;Find Files</source>
        <translation>搜尋檔案(&amp;F)</translation>
    </message>
    <message>
        <source>F3</source>
        <translation type="vanished">F3</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="93"/>
        <location filename="../../build/src/ui_main-win.h" line="797"/>
        <source>Filter by string...</source>
        <translation>按字串過濾...</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="117"/>
        <location filename="../../build/src/ui_main-win.h" line="798"/>
        <source>&amp;File</source>
        <translation>檔案(&amp;F)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="140"/>
        <location filename="../../build/src/ui_main-win.h" line="800"/>
        <source>&amp;Help</source>
        <translation>說明(&amp;H)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="146"/>
        <location filename="../../build/src/ui_main-win.h" line="801"/>
        <source>&amp;View</source>
        <translation>檢視(&amp;V)</translation>
    </message>
    <message>
        <source>Sorting</source>
        <translation type="obsolete">排序</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="176"/>
        <location filename="../../build/src/ui_main-win.h" line="803"/>
        <source>&amp;Edit</source>
        <translation>編輯(&amp;E)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="194"/>
        <location filename="../../build/src/ui_main-win.h" line="804"/>
        <source>&amp;Bookmarks</source>
        <translation>書籤(&amp;B)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="201"/>
        <location filename="../../build/src/ui_main-win.h" line="805"/>
        <source>&amp;Go</source>
        <translation>前往(&amp;G)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="222"/>
        <location filename="../../build/src/ui_main-win.h" line="806"/>
        <source>&amp;Tool</source>
        <translation>工具(&amp;T)</translation>
    </message>
</context>
<context>
    <name>MountOperationPasswordDialog</name>
    <message>
        <location filename="../mount-operation-password.ui" line="20"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="172"/>
        <source>Mount</source>
        <translation>掛載</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="48"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="174"/>
        <source>Connect &amp;anonymously</source>
        <translation>匿名連線(&amp;A)</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="58"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="175"/>
        <source>Connect as u&amp;ser:</source>
        <translation>以使用者帳號連線(&amp;S)：</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="79"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="176"/>
        <source>&amp;Username:</source>
        <translation>使用者名稱(&amp;U)：</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="102"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="177"/>
        <source>&amp;Password:</source>
        <translation>密碼(&amp;P)：</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="112"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="178"/>
        <source>&amp;Domain:</source>
        <translation>網域(&amp;D)：</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="127"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="179"/>
        <source>Forget password &amp;immediately</source>
        <translation>立刻忘記密碼(&amp;I)</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="137"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="180"/>
        <source>Remember password until you &amp;logout</source>
        <translation>記住密碼直到登出(&amp;L)</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="147"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="181"/>
        <source>Remember &amp;forever</source>
        <translation>永遠記住密碼(&amp;F)</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../preferences.ui" line="14"/>
        <location filename="../../build/src/ui_preferences.h" line="641"/>
        <source>Preferences</source>
        <translation>偏好設定</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="45"/>
        <location filename="../../build/src/ui_preferences.h" line="650"/>
        <source>User Interface</source>
        <translation>使用者界面</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="35"/>
        <location filename="../../build/src/ui_preferences.h" line="646"/>
        <source>Behavior</source>
        <translation>行為</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="40"/>
        <location filename="../../build/src/ui_preferences.h" line="648"/>
        <source>Display</source>
        <translation>顯示</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="50"/>
        <location filename="../preferences.ui" line="451"/>
        <location filename="../../build/src/ui_preferences.h" line="652"/>
        <location filename="../../build/src/ui_preferences.h" line="701"/>
        <source>Thumbnail</source>
        <translation>縮圖</translation>
    </message>
    <message>
        <source>Volume</source>
        <translation type="vanished">磁碟</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="55"/>
        <location filename="../../build/src/ui_preferences.h" line="654"/>
        <source>Advanced</source>
        <translation>進階</translation>
    </message>
    <message>
        <source>Bookmarks:</source>
        <translation type="vanished">書籤：</translation>
    </message>
    <message>
        <source>Open in current tab</source>
        <translation type="vanished">在目前分頁開啟</translation>
    </message>
    <message>
        <source>Open in new tab</source>
        <translation type="vanished">在新分頁開啟</translation>
    </message>
    <message>
        <source>Open in new window</source>
        <translation type="vanished">在新視窗開啟</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="125"/>
        <location filename="../../build/src/ui_preferences.h" line="663"/>
        <source>Erase files on removable media instead of &quot;trash can&quot; creation</source>
        <translation>擦除外接儲存裝置裡面的檔案，而並不是移動到 &quot;垃圾桶&quot; 上</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="132"/>
        <location filename="../../build/src/ui_preferences.h" line="664"/>
        <source>Confirm before moving files into &quot;trash can&quot;</source>
        <translation>將檔案移動至 &quot;垃圾桶&quot; 前進行確認</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="139"/>
        <location filename="../../build/src/ui_preferences.h" line="665"/>
        <source>Don&apos;t ask options on launch executable file</source>
        <translation>不要再詢問檔案執行選項</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="166"/>
        <location filename="../../build/src/ui_preferences.h" line="666"/>
        <source>Icons</source>
        <translation>圖示</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="195"/>
        <location filename="../../build/src/ui_preferences.h" line="668"/>
        <source>Size of big icons:</source>
        <translation>大圖示尺寸：</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="205"/>
        <location filename="../../build/src/ui_preferences.h" line="669"/>
        <source>Size of small icons:</source>
        <translation>小圖示尺寸：</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="215"/>
        <location filename="../../build/src/ui_preferences.h" line="670"/>
        <source>Size of thumbnails:</source>
        <translation>縮圖尺寸：</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="225"/>
        <location filename="../../build/src/ui_preferences.h" line="671"/>
        <source>Size of side pane icons:</source>
        <translation>側邊欄圖示尺寸:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="178"/>
        <location filename="../../build/src/ui_preferences.h" line="667"/>
        <source>Icon theme:</source>
        <translation>圖示主題：</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="92"/>
        <location filename="../../build/src/ui_preferences.h" line="659"/>
        <source>Save metadata to directories (.DirInfo files)</source>
        <translation>將中繼資料儲存到目錄 (.DirInfo 檔案)</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="102"/>
        <location filename="../../build/src/ui_preferences.h" line="660"/>
        <source>Spatial mode (folders open in a new window)</source>
        <translation>分層模式 (在新視窗中打開資料夾)</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="298"/>
        <location filename="../../build/src/ui_preferences.h" line="677"/>
        <source>Window</source>
        <translation>視窗</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="314"/>
        <location filename="../../build/src/ui_preferences.h" line="679"/>
        <source>Default width of new windows:</source>
        <translation>新視窗的預設寬度：</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="328"/>
        <location filename="../../build/src/ui_preferences.h" line="680"/>
        <source>Default height of new windows:</source>
        <translation>新視窗的預設高度：</translation>
    </message>
    <message>
        <source>Always show the tab bar</source>
        <translation type="vanished">總是顯示標籤列</translation>
    </message>
    <message>
        <source>Show &apos;Close&apos; buttons on tabs	</source>
        <translation type="vanished">在分頁標籤上顯示 &quot;關閉&quot; 按鈕	</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="307"/>
        <location filename="../../build/src/ui_preferences.h" line="678"/>
        <source>Remember the size of the last closed window</source>
        <translation>記住最後關閉視窗的大小</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="76"/>
        <location filename="../../build/src/ui_preferences.h" line="657"/>
        <source>Browsing</source>
        <translation>瀏覽</translation>
    </message>
    <message>
        <source>Open files with single click</source>
        <translation type="vanished">單擊開啟檔案</translation>
    </message>
    <message>
        <source>Delay of auto-selection in single click mode (0 to disable)</source>
        <translation type="vanished">單擊模式下自動選擇延遲(0為禁用)</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="82"/>
        <location filename="../../build/src/ui_preferences.h" line="658"/>
        <source>Default view mode:</source>
        <translation>預設檢視模式：</translation>
    </message>
    <message>
        <source> sec</source>
        <translation type="vanished"> 秒</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="112"/>
        <location filename="../../build/src/ui_preferences.h" line="661"/>
        <source>File Operations</source>
        <translation>檔案開啟方式</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="118"/>
        <location filename="../../build/src/ui_preferences.h" line="662"/>
        <source>Confirm before deleting files</source>
        <translation>刪除檔案前確認</translation>
    </message>
    <message>
        <source>Move deleted files to &quot;trash bin&quot; instead of erasing from disk.</source>
        <translation type="vanished">將檔案移動至垃圾桶，而不直接在磁碟中刪除。</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="238"/>
        <location filename="../../build/src/ui_preferences.h" line="672"/>
        <source>User interface</source>
        <translation>使用者界面</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="251"/>
        <location filename="../../build/src/ui_preferences.h" line="674"/>
        <source>Treat backup files as hidden</source>
        <translation>隱藏備份檔案</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="261"/>
        <location filename="../../build/src/ui_preferences.h" line="675"/>
        <source>Always show full file names</source>
        <translation>一律顯示完整檔案名稱</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="271"/>
        <location filename="../../build/src/ui_preferences.h" line="676"/>
        <source>Show icons of hidden files shadowed</source>
        <translation>顯示隱藏的陰影檔案圖示</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="345"/>
        <location filename="../../build/src/ui_preferences.h" line="681"/>
        <source>Show in places</source>
        <translation>在地方上顯示</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="352"/>
        <location filename="../../build/src/ui_preferences.h" line="686"/>
        <source>Home</source>
        <translation>家目錄</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="364"/>
        <location filename="../../build/src/ui_preferences.h" line="688"/>
        <source>Desktop</source>
        <translation>桌面</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="376"/>
        <location filename="../../build/src/ui_preferences.h" line="690"/>
        <source>Trash can</source>
        <translation>垃圾桶</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="388"/>
        <location filename="../../build/src/ui_preferences.h" line="692"/>
        <source>Computer</source>
        <translation>電腦</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="400"/>
        <location filename="../../build/src/ui_preferences.h" line="694"/>
        <source>Applications</source>
        <translation>程式</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="408"/>
        <location filename="../../build/src/ui_preferences.h" line="696"/>
        <source>Devices</source>
        <translation>裝置</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="416"/>
        <location filename="../../build/src/ui_preferences.h" line="698"/>
        <source>Network</source>
        <translation>網路</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="481"/>
        <location filename="../../build/src/ui_preferences.h" line="705"/>
        <source>Show thumbnails of files</source>
        <translation>顯示檔案縮圖</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="558"/>
        <location filename="../../build/src/ui_preferences.h" line="711"/>
        <source>Templates</source>
        <translation>模板</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="564"/>
        <location filename="../../build/src/ui_preferences.h" line="712"/>
        <source>Show only user defined templates in menu</source>
        <translation>在功能表中只顯示使用者定義的模板</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="571"/>
        <location filename="../../build/src/ui_preferences.h" line="713"/>
        <source>Show only one template for each MIME type</source>
        <translation>每個 MIME 類型僅顯示一個模板</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="578"/>
        <location filename="../../build/src/ui_preferences.h" line="714"/>
        <source>Run default application after creation from template</source>
        <translation>從模板建立後執行預設應用程序</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="474"/>
        <location filename="../../build/src/ui_preferences.h" line="704"/>
        <source>Only show thumbnails for local files</source>
        <translation>只顯示本地檔案的縮圖</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="457"/>
        <location filename="../../build/src/ui_preferences.h" line="702"/>
        <source>Do not generate thumbnails for image files exceeding this size:</source>
        <translation>不讓超過指定大小的檔案產生縮圖:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="464"/>
        <location filename="../../build/src/ui_preferences.h" line="703"/>
        <source> KB</source>
        <translation> KB</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="635"/>
        <location filename="../../build/src/ui_preferences.h" line="719"/>
        <source>When removable medium unmounted:</source>
        <translation>退出外接儲存裝置時:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="524"/>
        <location filename="../../build/src/ui_preferences.h" line="708"/>
        <source>Switch &amp;user command:</source>
        <translation>切換使用者的指令(&amp;U):</translation>
    </message>
    <message>
        <source>Archiver in&amp;tegration:</source>
        <translation type="vanished">壓縮程式整合(&amp;T):</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="517"/>
        <location filename="../../build/src/ui_preferences.h" line="707"/>
        <source>Terminal emulator:</source>
        <translation>終端機模擬器:</translation>
    </message>
    <message>
        <source>Do not generate thumbnails for files exceeding this size (KB):</source>
        <translation type="obsolete">不為超過指定大小的檔案產生縮圖 (KB)</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="605"/>
        <location filename="../../build/src/ui_preferences.h" line="715"/>
        <source>Auto Mount</source>
        <translation>自動掛載</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="611"/>
        <location filename="../../build/src/ui_preferences.h" line="716"/>
        <source>Mount mountable volumes automatically on program startup</source>
        <translation>掛載可掛載的磁碟</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="618"/>
        <location filename="../../build/src/ui_preferences.h" line="717"/>
        <source>Mount removable media automatically when they are inserted</source>
        <translation>插入外接媒體時自動掛載</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="625"/>
        <location filename="../../build/src/ui_preferences.h" line="718"/>
        <source>Show available options for removable media when they are inserted</source>
        <translation>插入外接媒體時顯示可用選項</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="641"/>
        <location filename="../../build/src/ui_preferences.h" line="720"/>
        <source>Close &amp;tab containing removable medium</source>
        <translation>關閉包含外接儲存裝置中的標籤(&amp;T)</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="648"/>
        <location filename="../../build/src/ui_preferences.h" line="721"/>
        <source>Chan&amp;ge folder in the tab to home folder</source>
        <translation>將選項卡中的檔案夾更改為家目錄檔案夾(&amp;G)</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="508"/>
        <location filename="../../build/src/ui_preferences.h" line="706"/>
        <source>Programs</source>
        <translation>程式</translation>
    </message>
    <message>
        <source>Terminal emulator command for directories:</source>
        <translation type="obsolete">用來開啟目錄的終端機模擬器指令:</translation>
    </message>
    <message>
        <source>Switch user command:</source>
        <translation type="vanished">切換使用者的指令:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="537"/>
        <location filename="../../build/src/ui_preferences.h" line="709"/>
        <source>Examples: &quot;xterm -e %s&quot; for terminal or &quot;gksu %s&quot; for switching user.
%s = the command line you want to execute with terminal or su.</source>
        <translation>範例: 終端機指令可用 &quot;xterm -e %s&quot; 而切換使用者指令可用 &quot;gksu %s&quot;。
%s = 想要用終端機或是切換使用者執行的指令。</translation>
    </message>
    <message>
        <source>Archiver integration:</source>
        <translation type="vanished">壓縮程式整合:</translation>
    </message>
    <message>
        <source>Terminal emulator command for programs:</source>
        <translation type="obsolete">用來執行程式的終端機模擬器指令:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="244"/>
        <location filename="../../build/src/ui_preferences.h" line="673"/>
        <source>Use SI decimal prefixes instead of IEC binary prefixes</source>
        <translation>使用SI十進位制而不是IEC二進位制</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../filelauncher.cpp" line="186"/>
        <location filename="../mountoperation.cpp" line="185"/>
        <location filename="../utilities.cpp" line="133"/>
        <location filename="../utilities.cpp" line="209"/>
        <location filename="../utilities.cpp" line="285"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <source>Rename File</source>
        <translation type="vanished">重新命名</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="128"/>
        <source>Please enter a new name:</source>
        <translation>請輸入一個新名稱：</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="133"/>
        <source>The startvolume cannot be renamed.</source>
        <translation>啟動卷不能重新命名。</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="221"/>
        <source>Create Folder</source>
        <translation>建立資料夾</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="226"/>
        <source>Please enter a new file name:</source>
        <translation>請輸入一個新檔案名稱：</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="231"/>
        <source>Please enter a new folder name:</source>
        <translation>請輸入一個新資料夾名稱：</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="232"/>
        <source>New folder</source>
        <translation>新資料夾</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="227"/>
        <source>New text file</source>
        <translation>新文字檔</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="127"/>
        <source>Rename</source>
        <translation>重新命名</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="237"/>
        <source>Enter a name for the new %1:</source>
        <translation>幫新的 %1 輸入一個名稱：</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="222"/>
        <source>Create File</source>
        <translation>建立檔案</translation>
    </message>
</context>
<context>
    <name>RenameDialog</name>
    <message>
        <location filename="../rename-dialog.ui" line="14"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="156"/>
        <source>Confirm to replace files</source>
        <translation>確認取代檔案</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="35"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="157"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;There is already a file with the same name in this location.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Do you want to replace the existing file?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;此位置已存在相同名稱的檔案&lt;/span&gt;&lt;/p&gt;&lt;p&gt;是否要取代現有檔案？&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="56"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="158"/>
        <source>dest</source>
        <translation>目的地</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="63"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="159"/>
        <source>with the following file?</source>
        <translation>使用以下檔案？</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="76"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="160"/>
        <source>src file info</source>
        <translation>檔案來源內容</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="89"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="161"/>
        <source>dest file info</source>
        <translation>檔案目的地內容</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="102"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="162"/>
        <source>src</source>
        <translation>來源</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="122"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="163"/>
        <source>&amp;File name:</source>
        <translation>檔名(&amp;F)：</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="137"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="164"/>
        <source>Apply this option to all existing files</source>
        <translation>套用這個選項到所有已存在的檔案</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../filesearch.ui" line="14"/>
        <location filename="../../build/src/ui_filesearch.h" line="398"/>
        <source>Search Files</source>
        <translation>搜尋檔案</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="29"/>
        <location filename="../../build/src/ui_filesearch.h" line="408"/>
        <source>Name/Location</source>
        <translation>名稱/位置</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="35"/>
        <location filename="../../build/src/ui_filesearch.h" line="399"/>
        <source>File Name Patterns:</source>
        <translation>檔案名稱圖案：</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="41"/>
        <location filename="../../build/src/ui_filesearch.h" line="400"/>
        <source>*</source>
        <translation>*</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="48"/>
        <location filename="../../build/src/ui_filesearch.h" line="401"/>
        <source>Case insensitive</source>
        <translation>不分大小寫</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="55"/>
        <location filename="../../build/src/ui_filesearch.h" line="402"/>
        <source>Use regular expression</source>
        <translation>用名稱排列</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="65"/>
        <location filename="../../build/src/ui_filesearch.h" line="403"/>
        <source>Places to Search:</source>
        <translation>搜尋位置：</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="78"/>
        <location filename="../../build/src/ui_filesearch.h" line="404"/>
        <source>&amp;Add</source>
        <translation>新增(&amp;A)</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="90"/>
        <location filename="../../build/src/ui_filesearch.h" line="405"/>
        <source>&amp;Remove</source>
        <translation>移除(&amp;R)</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="119"/>
        <location filename="../../build/src/ui_filesearch.h" line="406"/>
        <source>Search in sub directories</source>
        <translation>在子目錄中搜尋</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="126"/>
        <location filename="../../build/src/ui_filesearch.h" line="407"/>
        <source>Search for hidden files</source>
        <translation>搜尋隱藏檔案</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="137"/>
        <location filename="../../build/src/ui_filesearch.h" line="416"/>
        <source>File Type</source>
        <translation>檔案類型</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="143"/>
        <location filename="../../build/src/ui_filesearch.h" line="409"/>
        <source>Only search for files of following types:</source>
        <translation>只搜尋以下類型的檔案：</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="149"/>
        <location filename="../../build/src/ui_filesearch.h" line="410"/>
        <source>Text files</source>
        <translation>文字檔</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="156"/>
        <location filename="../../build/src/ui_filesearch.h" line="411"/>
        <source>Image files</source>
        <translation>圖片檔</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="163"/>
        <location filename="../../build/src/ui_filesearch.h" line="412"/>
        <source>Audio files</source>
        <translation>音訊檔</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="170"/>
        <location filename="../../build/src/ui_filesearch.h" line="413"/>
        <source>Video files</source>
        <translation>影像檔</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="177"/>
        <location filename="../../build/src/ui_filesearch.h" line="414"/>
        <source>Documents</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="184"/>
        <location filename="../../build/src/ui_filesearch.h" line="415"/>
        <source>Folders</source>
        <translation>資料夾</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="208"/>
        <location filename="../../build/src/ui_filesearch.h" line="420"/>
        <source>Content</source>
        <translation>內容</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="214"/>
        <location filename="../../build/src/ui_filesearch.h" line="417"/>
        <source>File contains:</source>
        <translation>檔案包含：</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="223"/>
        <location filename="../../build/src/ui_filesearch.h" line="418"/>
        <source>Case insensiti&amp;ve</source>
        <translation>不分大小寫(&amp;V)</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="230"/>
        <location filename="../../build/src/ui_filesearch.h" line="419"/>
        <source>&amp;Use regular expression</source>
        <translation>用名稱排列(&amp;U)</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="254"/>
        <location filename="../../build/src/ui_filesearch.h" line="437"/>
        <source>Properties</source>
        <translation>屬性</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="260"/>
        <location filename="../../build/src/ui_filesearch.h" line="421"/>
        <source>File Size:</source>
        <translation>檔案大小：</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="266"/>
        <location filename="../../build/src/ui_filesearch.h" line="422"/>
        <source>Larger than:</source>
        <translation>大於：</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="282"/>
        <location filename="../filesearch.ui" line="323"/>
        <location filename="../../build/src/ui_filesearch.h" line="423"/>
        <location filename="../../build/src/ui_filesearch.h" line="429"/>
        <source>Bytes</source>
        <translation>位元</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="287"/>
        <location filename="../filesearch.ui" line="328"/>
        <location filename="../../build/src/ui_filesearch.h" line="424"/>
        <location filename="../../build/src/ui_filesearch.h" line="430"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="292"/>
        <location filename="../filesearch.ui" line="333"/>
        <location filename="../../build/src/ui_filesearch.h" line="425"/>
        <location filename="../../build/src/ui_filesearch.h" line="431"/>
        <source>MiB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="297"/>
        <location filename="../filesearch.ui" line="338"/>
        <location filename="../../build/src/ui_filesearch.h" line="426"/>
        <location filename="../../build/src/ui_filesearch.h" line="432"/>
        <source>GiB</source>
        <translation>GiB</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="307"/>
        <location filename="../../build/src/ui_filesearch.h" line="428"/>
        <source>Smaller than:</source>
        <translation>小於：</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="351"/>
        <location filename="../../build/src/ui_filesearch.h" line="434"/>
        <source>Last Modified Time:</source>
        <translation>最後修改時間：</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="357"/>
        <location filename="../../build/src/ui_filesearch.h" line="435"/>
        <source>Earlier than:</source>
        <translation>早於：</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="364"/>
        <location filename="../../build/src/ui_filesearch.h" line="436"/>
        <source>Later than:</source>
        <translation>晚於：</translation>
    </message>
</context>
</TS>
