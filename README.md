# OpenFinder [![Build Status](https://api.cirrus-ci.com/github/helloSystem/Filer.svg)](https://cirrus-ci.com/github/helloSystem/Filer) [![Translation status](https://hosted.weblate.org/widgets/hellosystem/-/filer/svg-badge.svg)](https://hosted.weblate.org/engage/hellosystem/)

A file manager that also renders the desktop. A key component of [helloSystem](https://hellosystem.github.io/docs/).

## Features

* Appeal to users coming from macOS
* Cross platform: Builds for Linux, FreeBSD
* Spatial mode (each folder opens in its own window)
* Can handle [Application Bundles](https://hellosystem.github.io/docs/developer/application-bundles.html)
* Can handle ELF files that are lacking the executable bit (backlog)
* Can handle AppImages (backlog)
* Context menu can be extended using file manager actions
